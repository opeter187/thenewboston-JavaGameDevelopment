import javafx.beans.WeakInvalidationListener;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class ScreenManager {

    private GraphicsDevice vc;


    // give vc access to monitor screen
    public ScreenManager() {
        GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
        vc = e.getDefaultScreenDevice();

    }

    private DisplayMode[] getCompatibleDisplayModes() {
        return vc.getDisplayModes();
    }


    public DisplayMode findFirstCompatibleMode(DisplayMode modes[]) {
        DisplayMode goodModes[] = vc.getDisplayModes();

        for (int x = 0; x < modes.length; x++) {
            for (int y = 0; y < goodModes.length; y++) {
                if (displaModeMatch(modes[x], goodModes[y])) {
                    return modes[x];
                }
            }
        }
        return null;
    }

    public DisplayMode getCurrentDisplayMode() {
        return vc.getDisplayMode();
    }

    private boolean displaModeMatch(DisplayMode mode, DisplayMode goodMode) {
        return mode.equals(goodMode);
    }

    public void setFullScreen(DisplayMode dm) {
        JFrame f = new JFrame();
        f.setUndecorated(true);
        f.setIgnoreRepaint(true);
        f.setResizable(false);
        vc.setFullScreenWindow(f);

        if (dm != null && vc.isDisplayChangeSupported()) {
            try {
                vc.setDisplayMode(dm);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        f.createBufferStrategy(2);
    }


    public Graphics2D getGraphics() {
        Window w = vc.getFullScreenWindow();
        if (w != null) {
            BufferStrategy s = w.getBufferStrategy();
            return (Graphics2D) s.getDrawGraphics();
        }
        return null;
    }

    public void update() {
        Window w = vc.getFullScreenWindow();
        if (w != null) {
            BufferStrategy s = w.getBufferStrategy();
            if (!s.contentsLost()) {
                s.show();
            }
        }
    }


    public Window getFullSCreenWindow() {
        return vc.getFullScreenWindow();
    }

    public int getWith() {
        Window w = vc.getFullScreenWindow();
        if (w != null) {
            return w.getWidth();
        }
        return 0;
    }

    public int getHight() {
        Window w = vc.getFullScreenWindow();
        if (w != null) {
            return w.getHeight();
        }
        return 0;
    }

    public void restoreScreen() {
        Window w = vc.getFullScreenWindow();
        if (w != null) {
            w.dispose();
        }
        vc.setFullScreenWindow(null);
    }

    public BufferedImage createCompatibleImage(int w, int h, int t) {
        Window win = vc.getFullScreenWindow();
        if (win != null) {
            GraphicsConfiguration gc = win.getGraphicsConfiguration();
            return gc.createCompatibleImage(w, h, t);
        }
        return null;
    }
}
