import javax.swing.*;
import java.awt.*;

public class Bucky extends JFrame {

    private ScreenManager s;
    private Image bg;
    private Animation a;

    private static final DisplayMode[] modes1={
            new DisplayMode(1920,1080,32,0),
            new DisplayMode(1280,1080,32,0),
            new DisplayMode(1440,900,32,0), // this is for my macbook air
            new DisplayMode(1440,900,24,0),
            new DisplayMode(1440,900,16,0),
            new DisplayMode(800,600,32,0),
            new DisplayMode(800,600,24,0),
            new DisplayMode(800,600,16,0)
    };


    public static void main(String[] args) {
        Bucky b = new Bucky();
        b.run();
    }

    private void loadPics() {
        bg = new ImageIcon("resources/bg.jpg").getImage();
        Image face1 = new ImageIcon("resources/smile.png").getImage();
        Image face2 = new ImageIcon("resources/laugh.png").getImage();

        a = new Animation();
        a.addScene(face1, 250);
        a.addScene(face2, 250);
    }


    private void run() {
        s = new ScreenManager();
        try {
            DisplayMode dm = s.findFirstCompatibleMode(modes1);
            s.setFullScreen(dm);
            loadPics();
            movieLoop();
        } finally {
            s.restoreScreen();
        }
    }

    private void movieLoop() {
        long startintTime = System.currentTimeMillis();
        long cumTime = startintTime;

        while (cumTime - startintTime < 3000) {
            long timePassed = System.currentTimeMillis() - cumTime;
            cumTime += timePassed;
            a.update(timePassed);

            Graphics2D g = s.getGraphics();
            draw(g);
            g.dispose();
            s.update();

            try {
                Thread.sleep(20);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void draw(Graphics g) {
        g.drawImage(bg, 0,0,null);
        g.drawImage(a.getImage(), 100,100,null);

    }

}
